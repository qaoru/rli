---
title: Projet RLI
subtitle: Détection et Correction d'erreurs
date: Avril 2019
author: Paul HENG
titlepage: true
header-includes: |
    \usepackage{amsmath}
    \setcounter{MaxMatrixCols}{12}
---

# Introduction

blabla

# Ajouts

J'ai ajouté une fonction d'erreur pour faciliter les tests :
```includeError5``` fonctionne de manière similaire à la version 2, mais en
itérant avec un pas de 2 (comme pour la v1). Je l'ai rajoutée de sachant pas
si le pas de 1 était volontaire pour la v2 et pour pouvoir tester avec des cas 
aléatoires qui peuvent être corrigés à coup sûr.
Concrètement, c'est juste la boucle d'itération sur ```i``` qui a changé.
```c
for (i = 0; i < size; i += 2) {
    // ...
}
```

# Codage de Hamming

## Questions

1. Le k-ième bit d’une combinaison linéaire quelconque des k-1 premières lignes de G vaudra toujours 0.
2. Le poids minimum du code obtenu est de 3.
3. Avec le code de Hamming (7,4), on a :
$$
G =
\begin{bmatrix}
1 & 0 & 0 & 0 & 1 & 1 & 1\\
0 & 1 & 0 & 0 & 1 & 1 & 0\\
0 & 0 & 1 & 0 & 1 & 0 & 1\\
0 & 0 & 0 & 1 & 0 & 1 & 1\\
\end{bmatrix}
H=
\begin{bmatrix}
1 & 1 & 1 & 0 & 1 & 0 & 0\\
1 & 1 & 0 & 1 & 0 & 1 & 0\\
1 & 0 & 1 & 1 & 0 & 0 & 1\\
\end{bmatrix}
$$
$$
G'=
\begin{bmatrix}
1 & 0 & 0 & 1 & 1 & 1\\
0 & 1 & 0 & 1 & 1 & 0\\
0 & 0 & 1 & 1 & 0 & 1\\
\end{bmatrix}
H'=
\begin{bmatrix}
1 & 1 & 1 & 1 & 0 & 0\\
1 & 1 & 0 & 0 & 1 & 0\\
1 & 0 & 1 & 0 & 0 & 1\\
\end{bmatrix}
$$
C'est un code 1-correcteur.
4. On obtient un code dont le poids minimum est 3. C'est un code 2-détecteur et 1-correcteur.
5. Il y a plusieurs matrices $G$ possibles pour le code de Hamming (7,4). Elles sont obtenues en effectuant des
permutations sur les colonnes.
6. Il existe un code de Hamming (n,k) pour tout $n = 2^i - 1$ avec $k = 2^i - i - 1$.
7. Quelque soit $m$, on peut constuire un code 1-correcteur en prennant le premier code de Hamming (n,k) classique
ayant son $k \geq m$. On peut raccourcir ce code en supprimant $k - m$ lignes et colonnes de la matrice identité, et $k - m$ lignes de $A^T$

## Implémentation

Les matrices utilisées sont les suivantes :

$$
G=
\begin{bmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 0 & 0\\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 0\\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1\\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 0 & 1\\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 1 & 0 & 1 & 0\\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 1 & 0 & 1\\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & 1 & 1 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & 1 & 1\\
\end{bmatrix}
H=
\begin{bmatrix}
1 & 0 & 0 & 1 & 1 & 0 & 1 & 0 & 1 & 0 & 0 & 0\\
1 & 1 & 0 & 1 & 0 & 1 & 1 & 1 & 0 & 1 & 0 & 0\\
0 & 1 & 1 & 0 & 1 & 0 & 1 & 1 & 0 & 0 & 1 & 0\\
0 & 0 & 1 & 1 & 0 & 1 & 0 & 1 & 0 & 0 & 0 & 1\\
\end{bmatrix}
$$
