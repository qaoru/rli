/**
 * @file decoding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Functions to decipher the code words
 */

#include <stdio.h>
#include "codingdecoding.h"

void copyDataBitsDecoding(CodeWord_t *cw, char *message, int data_size)
{
  int i = 0;

  for(i=0; i<data_size; i++)
    {
      setNthBitW(&(message[i]), 1, getNthBit(cw[i], 1));
      setNthBitW(&(message[i]), 2, getNthBit(cw[i], 2));
      setNthBitW(&(message[i]), 3, getNthBit(cw[i], 3));
      setNthBitW(&(message[i]), 4, getNthBit(cw[i], 4));
      setNthBitW(&(message[i]), 5, getNthBit(cw[i], 5));
      setNthBitW(&(message[i]), 6, getNthBit(cw[i], 6));
      setNthBitW(&(message[i]), 7, getNthBit(cw[i], 7));
      setNthBitW(&(message[i]), 8, getNthBit(cw[i], 8));
    }
}

void errorCorrection(CodeWord_t *cw, int data_size) {
  
  (void) data_size;
  int i = 0;
  int j = 0;
  int k = 0;
  char sum = 0;
  short syn = 0; // syndrome

  // Hamming parity matrix
  short hamH[4][12] = {
    {1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0},
    {1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0},
    {0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0},
    {0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1},
  };

  // H columns mapped to values (control then info)
  short synMatrix[12] = {
    0b1000,
    0b0100,
    0b0010,
    0b0001,
    0b1100,
    0b0110,
    0b0011,
    0b1101,
    0b1010,
    0b0101,
    0b1110,
    0b0111
  };

  for (i = 0; i < data_size; i++) {
    printBits(cw[i], "received");
    syn = 0;
    for (k = 0; k < 4; k++) {
      sum = 0;

      for (j = 0; j < 8; j++) {
        sum ^= hamH[k][j] & getNthBit(cw[i], 8-j) ;
      }

      sum ^= getNthBit(cw[i], 12-k); // add the control bit pointed by Id
      syn = syn | sum<<(3-k);
    }
    // printBits(cw[i], "code");
    // printBits((short) syn, "syn");
    if (syn) { // if syn is not null, there is at least an error
      printBits(syn, "syn");
      for (j = 0; j < 12; j++) {
        if (synMatrix[j] == syn) { // search for matching pattern
          printf("error detected on word %d at bit %d\n", i, 12-j);
          setNthBitCW(&cw[i], 12-j, !getNthBit(cw[i], 12-j)); // invert bit
        }
      }
      printf("error detected on word %d\n", i);

    }
  }
  return;
}


/* ========== PARITY ========== */
// int thereIsError(CodeWord_t *cw, int data_size)
// {
//   int i = 0;
//   int j = 1;
//   int sum = 0;
//   int ret = 0;

//   for(i=0; i<data_size; i++) {
//     sum = 0;
//     for(j = 1; j <= 8; j++) {
//       sum ^= getNthBit(cw[i], j);
//     }
//     if (getNthBit(cw[i], 9) != sum) {
//       printf("error detected on word %d\n", i);
//       ret++;
//     }
//   }
//   return 0;
// }

/* ========== POLYNOMIAL ========== */
// int thereIsError(CodeWord_t *cw, int data_size)
// {
//   int ret = 0;
//   short r = 0;
//   int i = 0;
//   int j = 0;
//   short poly = 0x8E80;
//   for (i = 0; i < data_size; i++) {
//     r = (0 ^ cw[i]<<8) ^ (unsigned short) cw[i]>>8;
//     for (j = 1; j < 9; j++) {
//       if (r & 0x8000) {
//         r ^= poly;
//       }
//       r = r<<1;
//     }
//     if (r) {
//       printf("error detected of word %d\n", i);
//       ret = 1;
//     }
//   }
//   return ret;
// }


void decoding(char *cw, int cw_size, char *message, int *data_size)
{
  *data_size = cw_size / sizeof(CodeWord_t);

  //-- For error correction
  //-- to uncomment when complete and needed
  errorCorrection((CodeWord_t*)cw, *data_size);

  //-- For decoding
  copyDataBitsDecoding((CodeWord_t*)cw, message, *data_size);

  //-- For error detection
  //-- to uncomment when complete and needed
  // if (thereIsError((CodeWord_t*)cw, *data_size)) {
  //   printf("PARITY ERROR: \"%s\"\n", message);
  // }

  return;
}
